package com.mycompany.AgendaIps;

import com.google.gson.Gson;
import com.mycompany.AgendaIps.logica.PerfilUsuario;
import com.mycompany.AgendaIps.logica.TipoId;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication // Significa que es la clase main
@RestController // Indica que la clase es un API RET (Tranferencia de Estado Representacional) Agrega capa al HTTP
// RET Se basa en una tranferencia de recursos

public class AgendaIpsApplication {

    @Autowired //Permite la conexión del JdbcTemplate a la base de datos
    //JdbcTemplate jdbcTemplate;//Instancia del JdbcTemplate el cual es el equivalente al statement de la clase conexión
    PerfilUsuario e;
    /// Ventajas: manejo de excepciones controlado.
    // Evita el SQL Injection que pueden ingresar los hackers porque valida las entradas de los parametros.
    //@Autowired // Sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva. Equivalente a c = new Cuenta();
    //TipoId ti;

    public static void main(String[] args) {
        SpringApplication.run(AgendaIpsApplication.class, args);
    }

    @GetMapping("/hola")
    public String hola(@RequestParam(value = "name", defaultValue = "Jairito") String name,
             @RequestParam(value = "edad", defaultValue = "51") String edad) {
        return String.format("Hello %s, you are %s old!", name, edad);//El %s va a traer el valor
    }

    @GetMapping("/paciente")
    public String consultarPaciente(@RequestParam(value = "cedula", defaultValue = "pc") String cedula) throws ClassNotFoundException, SQLException {
        e.setIdperfilUsuario(cedula);
        if (e.consultar()) {
            return new Gson().toJson(e);
        } else {
            return new Gson().toJson(e);
        }
    }

}
