package com.mycompany.AgendaIps.logica;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * @author JaSoGo
 */
@Component("perfilUsuario")// en SpringBoot los modelos pasan a ser componentes
public class PerfilUsuario {

    @Autowired//Sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva (cuenta c =new cuenta();, ahora es cuenta c;)
    transient JdbcTemplate jdbcTemplate;//Reemplaza al statement de conexión
    // transient permite que el JSON ignore el campo JdbTemplate, si no lo hacemos lanzará un error de campos repetidos por el @Autowired
    // Atributos
    private String idperfilUsuario;
    private String nomPerfilusuario;

    public PerfilUsuario() {
    }

    public PerfilUsuario(String idperfilUsuario, String nombrePerfilusuario) {
        this.idperfilUsuario = idperfilUsuario;
        this.nomPerfilusuario = nombrePerfilusuario;
    }

    public String getIdperfilUsuario() {
        return idperfilUsuario;
    }

    public void setIdperfilUsuario(String idperfilUsuario) {
        this.idperfilUsuario = idperfilUsuario;
    }

    public String getNomPerfilusuario() {
        return nomPerfilusuario;
    }

    public void setNomPerfilusuario(String nombrePerfilusuario) {
        this.nomPerfilusuario = nombrePerfilusuario;
    }

    // CRUD
    // Guardar
    public String guardar() throws SQLException, ClassNotFoundException { // ClassNotFoundException
        String sql = "INSERT INTO perfilusuario (idperfilUsuario, nomPerfilusuario) VALUES (?,?);"; // consulta a la BD
        return sql;
    }

    // Consultar
    public boolean consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        String sql = "SELECT idPerfilUsuario, nomPerfilUsuario FROM perfilusuario WHERE idPerfilUsuario = ?";
        List<PerfilUsuario> perfilusuario = jdbcTemplate.query(sql, (rs, rowNum)
                -> new PerfilUsuario(
                        rs.getString("idPerfilUsuario"),
                        rs.getString("nomPerfilUsuario")
                ), new Object[]{this.getIdperfilUsuario()});
        if (perfilusuario != null && perfilusuario.size() > 0) {//Acá evaluamos si ese perfilUsuario existe
            this.setIdperfilUsuario(perfilusuario.get(0).getIdperfilUsuario());
            this.setNomPerfilusuario(perfilusuario.get(0).getNomPerfilusuario());
            return true;
         }else{
            return false;
        }
    }

    // Actualizar 
    public String actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U
        String sql = "UPDATE perfilusuario SET nomPerfilusuario = ? WHERE idPerfilUsuario = ?;";
        return sql;
    }

    // Borrar
    public String borrar() throws SQLException, ClassNotFoundException {
        //CRUD -D
        String sql = "DELETE FROM perfilusuario WHERE idPerfilUsuario = ?;";
        return sql;
    }

    // toString. para ver los valores de los atributos de la clase
    @Override
    public String toString() {
        return "perfilUsuario{" + "idperfilUsuario=" + idperfilUsuario + ", nomPerfilusuario=" + nomPerfilusuario + '}';
    }

}
