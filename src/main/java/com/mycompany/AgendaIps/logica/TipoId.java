package com.mycompany.AgendaIps.logica;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 * @author JaSoGo
 */
@Component("tipoId")// en SpringBoot los modelos pasan a ser componentes
public class TipoId {

    @Autowired//Sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva (cuenta c =new cuenta();, ahora es cuenta c;)
    JdbcTemplate jdbcTemplate;//Reemplaza al statement de conexión
    // Atributos
    private String idTipoId;
    private String nomTipoId;

    public TipoId() {
    }

    public TipoId(String idTipoId, String nomTipoId) {
        this.idTipoId = idTipoId;
        this.nomTipoId = nomTipoId;
    }

    public String getIdTipoId() {
        return idTipoId;
    }

    public void setIdTipoId(String idTipoId) {
        this.idTipoId = idTipoId;
    }

    public String getNomTipoId() {
        return nomTipoId;
    }

    public void setNomTipoId(String nomTipoId) {
        this.nomTipoId = nomTipoId;
    }

    // CRUD
    // Guardar
    public String guardar() throws SQLException, ClassNotFoundException { // ClassNotFoundException
        String sql = "INSERT INTO tipoId (idTipoId, nomTipoId) VALUES (?,?);"; // consulta a la BD
        return sql;
    }

    // Consultar
    public boolean consultar() throws ClassNotFoundException, SQLException {
        //CRUD -R 
        String sql = "SELECT idTipoId, nomTipoId FROM tipoId WHERE idTipoId = ?";
        List<TipoId> tipoId = jdbcTemplate.query(sql, (rs, rowNum)
                -> new TipoId(
                        rs.getString("idTipoId"),
                        rs.getString("nomTipoId")
                ), new Object[]{this.getIdTipoId()});
        if (tipoId != null && tipoId.size() > 0) {//Acá evaluamos si ese perfilUsuario existe
            this.setIdTipoId(tipoId.get(0).getIdTipoId());
            this.setNomTipoId(tipoId.get(0).getNomTipoId());
            return true;
         }else{
            return false;
        }
    }

    // Actualizar 
    public String actualizar() throws ClassNotFoundException, SQLException {
        // CRUD -U
        String sql = "UPDATE tipoId SET nomTipoId = '" + getNomTipoId() + "' WHERE idTipoId =?;";
        return sql;
    }

    // Borrar
    public String borrar() throws SQLException, ClassNotFoundException {
        //CRUD -D
        String sql = "DELETE FROM tipoId WHERE idTipoId =?;";
        return sql;
    }

    // toString. para ver los valores de los atributos de la clase
    @Override
    public String toString() {
        return "tipoId{" + "idTipoId=" + idTipoId + ", nomTipoId=" + nomTipoId + '}';
    }
}
